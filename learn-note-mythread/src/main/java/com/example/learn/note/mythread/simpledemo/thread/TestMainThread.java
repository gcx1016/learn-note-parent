package com.example.learn.note.mythread.simpledemo.thread;

/**
 * 入门demo1、
 * 每个进程执行时至少会有一个线程，本例中打印出了当前正在运行的线程名称 ‘main’
 * 该线程由jvm创建
 */
public class TestMainThread {

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
    }
}
