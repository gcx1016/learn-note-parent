package com.example.learn.note.mythread.simpledemo.runnable;

public class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("当前运行线程..." + Thread.currentThread().getName());
    }
}
