package com.example.learn.note.mythread.simpledemo;

public class MyThread2 extends Thread{

    private int count = 5;

    @Override
    public void run() {
        count--;
        System.out.println("由线程"+ this.currentThread().getName() + "计算，count=" + count);
    }
}
