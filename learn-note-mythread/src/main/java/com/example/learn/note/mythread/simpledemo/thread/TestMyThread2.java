package com.example.learn.note.mythread.simpledemo.thread;

/**
 * 线程的随机性：
 * 打印的两个线程内容是随机的，CPU具体执行哪个线程是不确定的。
 */
public class TestMyThread2 {

    public static void main(String[] args) {
        try {
            MyThread2 myThread = new MyThread2();
            myThread.setName("myThread");
            /**
             * 启动线程，异步执行，两个线程
             * 调用线程的start方法是表示此线程已准备就绪，等待CPU调用执行线程中的run方法（myThread线程调用）
             */
            //myThread.start();

            /**
             * 同步顺序执行，一个线程
             * 但是手动调用run方法（意思是main主线程调用），就变成代码同步顺序执行，
             * 并没有使用到多线程，观察打印的日志发现线程名称都是一样的
             */
            myThread.run();
            for (int i = 0; i < 10; i++) {
                int times = (int)(Math.random() * 1000);
                Thread.sleep(times);
                System.out.println("main=====" + Thread.currentThread().getName() + "=====" + i);
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
