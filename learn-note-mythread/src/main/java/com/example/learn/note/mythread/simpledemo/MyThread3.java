package com.example.learn.note.mythread.simpledemo;

public class MyThread3 extends Thread{

    private int count = 5;

    @Override
    synchronized public void run() {
        count--;
        System.out.println("由线程"+ this.currentThread().getName() + "计算，count=" + count);
    }
}
