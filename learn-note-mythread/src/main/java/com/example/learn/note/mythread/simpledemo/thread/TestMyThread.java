package com.example.learn.note.mythread.simpledemo.thread;

/**
 * 线程调用的随机性：
 * 分别在TestMyThread和MyThread中打印运行线程名称，发现打印结果有两个线程在运行
 * 且打印名称的先后顺序不一定，即代码中的执行顺序和线程的调用是顺序无关
 */
public class TestMyThread {

    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        myThread.start();
        System.out.println("TestMyThread..." + Thread.currentThread().getName());
    }
}
