package com.example.learn.note.mythread.simpledemo.thread;

/**
 * 实现多线程的方式一：
 *      继承Thread类，重写run方法
 */
public class MyThread3 extends Thread {

    private int i;

    MyThread3(int i) {
        super();
        this.i = i;
    }

    @Override
    public void run() {
        System.out.println(i);
    }
}
