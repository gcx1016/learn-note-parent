package com.example.learn.note.mythread.simpledemo.thread;

/**
 * 实现多线程的方式一：
 *      继承Thread类，重写run方法
 */
public class MyThread extends Thread {

    @Override
    public void run() {
        super.run();
        System.out.println("MyThread..." + Thread.currentThread().getName());
    }
}
