package com.example.learn.note.mythread.simpledemo.runnable;

/**
 * 实现多线程方式二：实现runnable接口
 * 由于继承Thread类有局限性（Java不支持多继承）所以可以选择实现runnable接口来实现多线程
 */
public class TestMainRunnable {

    public static void main(String[] args) {
        Runnable runnable = new MyRunnable();
        Thread thread = new Thread(runnable);
        thread.setName("runabble");
        thread.start();
        System.out.println("当前运行线程..." + Thread.currentThread().getName());
    }
}
