package com.example.learn.note.mythread;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnNoteMythreadApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnNoteMythreadApplication.class, args);
    }

}
