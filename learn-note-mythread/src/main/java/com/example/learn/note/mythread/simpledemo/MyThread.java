package com.example.learn.note.mythread.simpledemo;

public class MyThread extends Thread{

    private int count = 5;

    MyThread(String name) {
        super();
        this.setName(name);
    }

    @Override
    public void run() {
        while (count > 0) {
            count--;
            System.out.println("由线程"+ this.currentThread().getName() + "计算，count=" + count);
        }
    }
}
