package com.example.learn.note.mythread.simpledemo.thread;

/**
 * 线程的启动顺序与start()执行顺序无关
 * start的顺序并不等于线程的启动顺序，执行哪个线程由CPU决定
 */
public class TestMyThread3 {

    public static void main(String[] args) {
        MyThread3 t1 = new MyThread3(1);
        MyThread3 t2 = new MyThread3(2);
        MyThread3 t3 = new MyThread3(3);
        MyThread3 t4 = new MyThread3(4);
        MyThread3 t5 = new MyThread3(5);
        MyThread3 t6 = new MyThread3(6);
        MyThread3 t7 = new MyThread3(7);
        MyThread3 t8 = new MyThread3(8);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();

    }
}
