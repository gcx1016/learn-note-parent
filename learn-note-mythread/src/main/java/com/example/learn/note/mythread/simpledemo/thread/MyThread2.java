package com.example.learn.note.mythread.simpledemo.thread;

/**
 * 实现多线程的方式一：
 *      继承Thread类，重写run方法
 */
public class MyThread2 extends Thread {

    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                int times = (int)(Math.random() * 1000);
                Thread.sleep(times);
                System.out.println("run=====" + Thread.currentThread().getName() + "=====" + i);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
