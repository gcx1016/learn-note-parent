package com.example.learn.note.mythread.simpledemo;

import org.junit.Test;

public class TestMyThread {

    /**
     * 线程不共享变量示例
     */
    @Test
    public void test1() {

        /**
         * 如果线程A、B、C表示3家商店，count=5表示每家都有同样的商品5件，
         * 各家店售卖互相没有关系，则变量不共享
         */
        MyThread myThread1 = new MyThread("A");
        MyThread myThread2 = new MyThread("B");
        MyThread myThread3 = new MyThread("C");

        myThread1.start();
        myThread2.start();
        myThread3.start();
    }

    /**
     * 线程共享变量示例（非线程安全）
     */
    @Test
    public void test2() {
        /**
         * 如果线程A、B、C表示一家商店3个售货员，count=5表示商品只有5件，
         * 则每个售货员售卖的数量就互相有联系，则变量共享
         */
        MyThread2 myThread = new MyThread2();

       /* Thread thread1 = new Thread(myThread, "A");
        Thread thread2 = new Thread(myThread, "B");
        Thread thread3 = new Thread(myThread, "C");*/

        MyThread2 thread1 = new MyThread2();
        thread1.setName("A");
        MyThread2 thread2 = new MyThread2();
        thread2.setName("B");
        MyThread2 thread3 = new MyThread2();
        thread3.setName("C");

        thread1.start();
        thread2.start();
        thread3.start();
    }

    /**
     * 线程共享变量示例（线程安全）
     */
    @Test
    public void test3() {
        /**
         * 如果线程A、B、C表示一家商店3个售货员，count=5表示商品只有5件，
         * 则每个售货员售卖的数量就互相有联系，则变量共享
         */
        MyThread3 myThread = new MyThread3();

        /*Thread thread1 = new Thread(myThread, "A");
        Thread thread2 = new Thread(myThread, "B");
        Thread thread3 = new Thread(myThread, "C");*/

        MyThread3 thread1 = new MyThread3();
        thread1.setName("A");
        MyThread3 thread2 = new MyThread3();
        thread2.setName("B");
        MyThread3 thread3 = new MyThread3();
        thread3.setName("C");

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
