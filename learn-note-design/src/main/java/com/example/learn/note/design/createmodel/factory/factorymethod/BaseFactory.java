package com.example.learn.note.design.createmodel.factory.factorymethod;

import com.example.learn.note.design.createmodel.factory.simpfactory.AliBusiness;
import com.example.learn.note.design.createmodel.factory.simpfactory.JDBusiness;
import com.example.learn.note.design.createmodel.factory.simpfactory.PddBusiness;

public class BaseFactory {

    public static BusinessFactory createFactory(String type) {
        if ("jd".equalsIgnoreCase(type)) {
            // 不只是简单的new对象，可以做其他的逻辑处理，处理完返回对象即可
            return new JDBusinessFactory();
        } else if ("pdd".equalsIgnoreCase(type)) {
            return new PddBusinessFactory();
        } else if ("ali".equalsIgnoreCase(type)) {
            return new AliBusinessFactory();
        } else {
            return null;
        }
    }
}
