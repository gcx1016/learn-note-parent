package com.example.learn.note.design.createmodel.singleton;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 单例设计模式之枚举
 *     构造函数私有，不能通过new方式获取
 *     提供公有的方法供外部获取实例
 */
public enum Singleton5 {

    SINGLETON_5;

    private AtomicLong id = new AtomicLong(0);

    public long getId() {
        return id.incrementAndGet();
    }
}
