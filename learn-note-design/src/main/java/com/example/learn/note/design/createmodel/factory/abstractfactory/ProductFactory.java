package com.example.learn.note.design.createmodel.factory.abstractfactory;

/**
 *
 */
public interface ProductFactory {

    /**
     *
     * @return
     */
   Business createBuiness();

    /**
     *
     * @return
     */
   Merchants createMerchant();
}
