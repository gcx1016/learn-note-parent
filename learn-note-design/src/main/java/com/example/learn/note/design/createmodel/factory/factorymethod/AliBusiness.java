package com.example.learn.note.design.createmodel.factory.factorymethod;

import com.example.learn.note.design.createmodel.factory.factorymethod.Business;

/**
 * 具体产品,某个电商平台
 */
public class AliBusiness implements Business {
    @Override
    public int flow() {
        return 10000;
    }
}
