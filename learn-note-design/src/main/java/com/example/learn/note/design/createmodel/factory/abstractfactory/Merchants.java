package com.example.learn.note.design.createmodel.factory.abstractfactory;

/**
 * 抽象产品一：
 *      商户
 */
public interface Merchants {

    /**
     * 商户名称
     * @return
     */
    String merchantsName();
}
