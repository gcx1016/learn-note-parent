package com.example.learn.note.design.createmodel.factory.factorymethod;

/**
 *
 */
public class AliBusinessFactory implements BusinessFactory{
    @Override
    public Business create() {
        return new AliBusiness();
    }
}
