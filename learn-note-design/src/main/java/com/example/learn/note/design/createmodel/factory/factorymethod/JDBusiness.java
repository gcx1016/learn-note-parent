package com.example.learn.note.design.createmodel.factory.factorymethod;

import com.example.learn.note.design.createmodel.factory.factorymethod.Business;

/**
 * 具体产品,某个电商平台
 */
public class JDBusiness implements Business {
    @Override
    public int flow() {
        return 100;
    }
}
