package com.example.learn.note.design.createmodel.singleton;

/**
 * 单例设计模式之双重检测
 *     1.构造函数私有，不能通过new方式获取
 *     2.提供公有的方法供外部获取实例
 *  即支持延时加载又支持高并发
 */
public class Singleton3 {

    /**
     * volatile防止JVM指令重排带来错误
     */
    private static volatile Singleton3 instance;

    /**
     * 私有化构造函数
     */
    private Singleton3(){}

    /**
     * 提供公有方法供外部获取实例
     * @return
     */
    public static Singleton3 getInstance(){
        if (instance == null) {
            synchronized (Singleton3.class) {
                if (instance == null) {
                    instance = new Singleton3();
                }
            }
        }
        return instance;
    }
}
