package com.example.learn.note.design.createmodel.singleton;

/**
 * 单例设计模式之懒汉式
 *     1.构造函数私有，不能通过new方式获取
 *     2.提供公有的方法供外部获取实例,不在静态属性中实例化类，在使用时才加载类，支持延时加载，对象创建线程安全
 *  缺点：有性能问题，不支持高并发，每个线程都会有加锁释放锁的动作
 */
public class Singleton2 {

    private static Singleton2 instance;

    /**
     * 私有化构造函数
     */
    private Singleton2(){}

    /**
     * 提供公有方法供外部获取实例（支持延迟加载）
     * synchronized加在方法上，每个线程都会先加锁，再释放锁
     * @return
     */
    public static synchronized Singleton2 getInstance(){
        if (instance == null) {
            instance = new Singleton2();
        }
        return instance;
    }
}
