package com.example.learn.note.design.createmodel.singleton;

/**
 * 单例设计模式之饿汉式
 *     1.构造函数私有，不能通过new方式获取
 *     2.提供公有的方法供外部获取实例，提供静态常量使类在加载时就创建唯一实例，对象创建时线程安全
 * 缺点：不支持延时加载
 *
 */
public class Singleton1 {

    /**
     * 类在加载的时候就已经创建并初始化好了实例（缺点：不支持延迟加载）
     */
    private static final Singleton1 INSTANCE = new Singleton1();

    /**
     * 私有化构造函数
     */
    private Singleton1(){}

    /**
     * 提供公有方法供外部获取实例
     * @return
     */
    public static Singleton1 getInstance(){
        return INSTANCE;
    }
}
