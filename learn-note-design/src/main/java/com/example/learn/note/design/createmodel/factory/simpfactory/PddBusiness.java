package com.example.learn.note.design.createmodel.factory.simpfactory;

/**
 * 具体产品,某个电商平台
 */
public class PddBusiness implements Business {
    @Override
    public int flow() {
        return 1000;
    }
}
