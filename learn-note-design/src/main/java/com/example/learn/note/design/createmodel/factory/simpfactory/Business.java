package com.example.learn.note.design.createmodel.factory.simpfactory;

/**
 * 抽象产品
 *      电商
 */
public interface Business {

    /**
     * 每个电商平台的流量
     * @return
     */
    int flow();
}
