package com.example.learn.note.design.createmodel.factory.factorymethod;

public class JDBusinessFactory implements BusinessFactory {
    @Override
    public Business create() {
        return new JDBusiness();
    }
}
