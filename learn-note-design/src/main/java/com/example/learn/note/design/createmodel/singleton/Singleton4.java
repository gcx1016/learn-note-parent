package com.example.learn.note.design.createmodel.singleton;

/**
 * 单例设计模式之静态内部类
 *     构造函数私有，不能通过new方式获取
 *     提供公有的方法供外部获取实例
 * 特点：即保证线程安全又支持延时加载
 */
public class Singleton4 {

    /**
     * 私有化构造函数
     */
    private Singleton4(){}

    /**
     * 静态内部类，外部类加载时并不会创建Singleton实例对象，只有调用getInstance时才会创建
     * 静态内部类虽然保证了单例在多线程并发下的线程安全性，但是在遇到序列化对象时，默认的方式运行得到的结果就是多例的。使用时请注意。
     */
    private static class Singleton {
        private static final Singleton4 instance = new Singleton4();
    }

    /**
     * 提供公有方法供外部获取实例
     * @return
     */
    public static synchronized Singleton4 getInstance(){
        return Singleton.instance;
    }
}
