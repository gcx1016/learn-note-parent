package com.example.learn.note.design.createmodel.factory.factorymethod;

/**
 * 抽象的工厂类
 */
public interface BusinessFactory {

    Business create();
}
