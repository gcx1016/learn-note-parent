package com.example.learn.note.design.createmodel.factory.simpfactory;

/**
 * 简单工厂的使用
 */
public class Test {

    public static void main(String[] args) {

        // 使用时调用工厂类的静态方法，根据传入的不同值返回相应的对象
        Business jd = BusinessFactory.create("jd");
        System.out.println("流量 " + jd.flow());
    }
}
