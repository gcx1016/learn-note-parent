package com.example.learn.note.design.createmodel.factory.simpfactory;

/**
 * 简单工厂：
 *      1.一个抽象的产品（接口或抽象类）
 *      2.多个产品
 *      3.工厂类，将创建对象的过程放到工厂类中，由于创建对象方法一般为静态方法，简单工厂模式又称为静态工厂
 *
 *  优点：
 *
 *  缺点：
 *      当想要新增一个产品时，需要修改工厂类中逻辑，扩展性不好
 */
public class BusinessFactory {

    public static Business create(String type){
        if ("jd".equalsIgnoreCase(type)) {
            // 不只是简单的new对象，可以做其他的逻辑处理，处理完返回对象即可
            return new JDBusiness();
        } else if ("pdd".equalsIgnoreCase(type)) {
            return new PddBusiness();
        } else if ("ali".equalsIgnoreCase(type)) {
            return new AliBusiness();
        } else {
            return null;
        }
    }
}
