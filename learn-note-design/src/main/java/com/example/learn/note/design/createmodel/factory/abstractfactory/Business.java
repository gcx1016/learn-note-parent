package com.example.learn.note.design.createmodel.factory.abstractfactory;

/**
 * 抽象产品二：
 *      电商平台
 */
public interface Business {

    /**
     * 每个电商平台的流量
     * @return
     */
    int flow();

    /**
     * 每个电商平台上商户数量
     * @return
     */
    int merchantsNum();
}
