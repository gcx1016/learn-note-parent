package com.example.learn.note.design;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnNoteDesignApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnNoteDesignApplication.class, args);
    }

}
