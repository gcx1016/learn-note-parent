package com.example.learn.note.jvm;

/**
 * 在jvm中每一个main线程会拥有线程独有的
 *       虚拟机栈、本地方法栈、程序计数器
 *  所有线程公有
 *       方法区和堆
 *
 *  每执行到一个方法，都会压入一个栈帧
 *      栈帧包括：
 *          局部变量表、操作数栈、动态连接、完成出口
 *
 *  该段代码将会包含main栈帧、A栈帧、B栈帧、C栈帧
 *
 */
public class MethodAndStack {

    public static void main(String[] args) {
        A();
    }

    public static void A(){
        B();
    }

    public static void B(){
        C();
    }

    public static void C(){
    }
}
