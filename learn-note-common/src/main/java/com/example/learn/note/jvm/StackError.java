package com.example.learn.note.jvm;

/**
 * StackOverflowError
 * 运行时设置JVM参数  -Xss1k
 * 该段代码A方法一直入栈不出栈，造成线程栈溢出
 *
 * VM参数：
 * -Xss
 *   设置线程堆栈大小（以字节为单位）。附加字母k或K表示KB，m或M表示MB，g或G表示GB。默认值取决于平台：
 *      Linux / ARM（32位）：320 KB
 *      Linux / i386（32位）：320 KB
 *      Linux / x64（64位）：1024 KB
 *      OS X（64位）：1024 KB
 *      Oracle Solaris / i386（32位）：320 KB
 *      Oracle Solaris / x64（64位）：1024 KB
 *   下面的示例以不同的单位将线程堆栈大小设置为1024 KB：
 *      -Xss1M
 *      -Xss1024k
 *      -Xss1048576
 *   此选项等效于-XX:ThreadStackSize
 *
 * -XX：ThreadStackSize = 大小
 *   设置线程堆栈大小（以字节为单位）。在字母后面加上k或K表示千字节，m或M表示兆字节，g或G表示千兆字节。默认值取决于平台：
 *       Linux / ARM（32位）：320 KB
 *       Linux / i386（32位）：320 KB
 *       Linux / x64（64位）：1024 KB
 *       OS X（64位）：1024 KB
 *       Oracle Solaris / i386（32位）：320 KB
 *       Oracle Solaris / x64（64位）：1024 KB
 *   以下示例显示如何以不同单位将线程堆栈大小设置为1024 KB：
 *       -XX：ThreadStackSize = 1m
 *      -XX：ThreadStackSize = 1024k
 *      -XX：ThreadStackSize = 1048576
 *   此选项等效于-Xss。
 *
 */
public class StackError {

    public static void main(String[] args) {
        A();
    }

    public static void A(){
        A();
    }
}
