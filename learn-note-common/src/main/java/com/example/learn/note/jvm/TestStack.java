package com.example.learn.note.jvm;

public class TestStack {

    public static void main(String[] args) {

        // 对应main中code为0和1的指令
        int a = 1;
        // 对应main中code为2和3的指令
        int b = 2;
        test(a, b);
    }

    public static int test(int a, int b){
        // 对应test中code为0和2的指令
        int c = 10;
        // 对应test中code为3和4的指令
        int d = 2;
        int e = (a + b) * c / d;
        return e;
    }
}
