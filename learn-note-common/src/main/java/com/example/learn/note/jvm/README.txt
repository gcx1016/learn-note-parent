一、JVM内存区域
        （虚拟内存、JVM运行时数据区）
            线程共享：生命周期随虚拟机的启动/关闭而创建/销毁。
                方法区、堆
                jdk1.7中永久代和jdk1.8中元空间都是方法区的一种实现（即是同一个东西）
            线程私有：生命周期与线程相同, 依赖用户线程的启动/结束 而 创建/销毁
                虚拟机栈、本地方法栈、程序计数器
        （物理内存）
            直接内存（堆外内存）
    1.虚拟机栈
        每一个线程会独有一个虚拟机栈的内存，每一个方法的调用到结束都对应一个栈帧的入栈与出栈）
        栈帧：
            是用来存储数据和部分过程结果的数据结构，同时也被用来处理动态链接、方法返回值和异常分派
            局部变量表、操作数栈、动态连接、完成出口
            局部变量表：存放局部变量
            操作数栈：
                类似缓存
                JVM类似操作系统，而操作系统主要包括：CPU + 缓存 + 内存，系统处理数据流程
                       数据---内存----缓存----CPU----缓存-----内存
                同理，在JVM中包括：执行引擎 + 操作数栈 + 堆栈等内存，系统处理数据流程
                       数据---堆栈内存---操作数栈---执行引擎---操作数栈---堆栈内存
            动态连接：
            完成出口：
    2.程序计数器
        每个线程独有，互不影响，记录当前执行的字节码的位置，占用内存很小，是唯一不会发生OOM的内存区域
        程序执行时是根据时间片的运行，有可能时间片到了程序没有执行完就需要挂起，所有需要记录程序当前执行到哪里了，方便下次接着执行
    3.本地方法栈
        和虚拟机栈类似，但服务的对象不是普通方法，而是native修饰的方法
    4.堆
        堆是 JVM 上最大的内存区域，我们申请的几乎所有的对象，都是在这里存储的。我们常说的垃圾回收，操作的对象就是堆。
            堆空间一般是程序启动时，就申请了，但是并不一定会全部使用。堆一般设置成可伸缩的。 随着对象的频繁创建，堆空间占用的越来越多，就需要不定期的对不再使用的对象进行回收。
            这个在 Java 中，就叫作 GC（GarbageCollection）。 那一个对象创建的时候，到底是在堆上分配，还是在栈上分配呢？这和两个方面有关：对象的类型和在 Java 类中存在的位置。
            Java 的对象可以分为基本数据类型和普通对象。 对于普通对象来说，JVM 会首先在堆上创建对象，然后在其他地方使用的其实是它的引用。
            比如，把这个引用保存在虚拟机栈的局部变量表中。 对于基本数据类型来说（byte、short、int、long、float、double、char)，有两种情况。
            当你在方法体内声明了基本数据类型的对象，它就会在栈上直接分配。其他情况，都是在堆上分配。
    5.方法区
        方法区（Method Area）是可供各条线程共享的运行时内存区域。
        它存储了每一个类的结构信息，例如运行时常量池（Runtime Constant Pool） 字段和方法数据、构造函数和普通方法的字节码内容、还包括一些在类、实例、接口初始化时用到的特殊方法
        方法区是 JVM 对内存的“逻辑划分”，在 JDK1.7 及之前很多开发者都习惯将方法区称为“永久代”，
        是因为在 HotSpot 虚拟机中，设计人员使用了永 久代来实现了 JVM 规范的方法区。在 JDK1.8 及以后使用了元空间来实现方法区

        Java8 为什么使用元空间替代永久代，这样做有什么好处呢？
        官方给出的解释是： 移除永久代是为了融合 HotSpotJVM 与 JRockitVM 而做出的努力，因为 JRockit 没有永久代，所以不需要配置永久代。
        永久代内存经常不够用或发生内存溢出，抛出异常 java.lang.OutOfMemoryError:PermGen。这是因为在 JDK1.7 版本中，指定的 PermGen 区大小为8M，
        由于 PermGen 中类的元数据信息在每次 FullGC 的时候都可能被收集，回收率都偏低，成绩很难令人满意；
        还有为 PermGen 分配多大的空间很难 确定，PermSize 的大小依赖于很多因素，比如，JVM 加载的 class 总数、常量池的大小和方法的大小等。

    6.直接内存
        JVM 在运行时，会从操作系统申请大块的堆内存，进行数据的存储；同时还有虚拟机栈、本地方法栈和程序计数器，这块称之为栈区。操作系统剩余的 内存也就是堆外内存。
        它不是虚拟机运行时数据区的一部分，也不是 java 虚拟机规范中定义的内存区域；如果使用了 NIO,这块区域会被频繁使用，在 java 堆内可以用directByteBuffer 对象直接引用并操作；
        这块内存不受 java 堆大小限制，但受本机总内存的限制，可以通过-XX:MaxDirectMemorySize 来设置（默认与堆内存最大值一样），所以也会出现 OOM 异 常。