package com.example.learn.note;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnNoteCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnNoteCommonApplication.class, args);
    }

}
