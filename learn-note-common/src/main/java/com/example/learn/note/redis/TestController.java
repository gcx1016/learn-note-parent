package com.example.learn.note.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisCluster;

@RestController
public class TestController {

    @Autowired
    private JedisCluster jedisCluster;

    @RequestMapping("/learn/note/redis/test.json")
    public void test() {

        int okNum = 0;
        for (int i = 0; i < 200000; i++) {
            if ("OK".equals(jedisCluster.set(String.valueOf(i), "world"))) {
                okNum++;
            }
        }

        System.out.println("okNum" + okNum);
    }
}
