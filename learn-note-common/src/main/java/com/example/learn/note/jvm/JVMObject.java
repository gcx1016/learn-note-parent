package com.example.learn.note.jvm;

public class JVMObject {

    public final static String MAN_TYPE="man";
    public static String WOMAN_TYPE="woman";

    public static void main(String[] args) throws InterruptedException {
        Teacher T1 = new Teacher();
        T1.setAge(10);
        T1.setName("t1");
        T1.setSexType(MAN_TYPE);

        Teacher T2 = new Teacher();
        T2.setAge(20);
        T2.setName("t2");
        T2.setSexType(WOMAN_TYPE);

        Thread.sleep(Integer.MAX_VALUE);
    }
}

class Teacher{
    String name;
    String sexType;
    int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSexType() {
        return sexType;
    }

    public void setSexType(String sexType) {
        this.sexType = sexType;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
